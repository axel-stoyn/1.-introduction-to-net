﻿using Hello.ErrorNotification;
using System;

namespace Hello
{
    //<summary>
    //Introduce person.
    //</summary>
    internal static class Welcome
    {
        //<summary>
        //Get Welcom message.
        //</summary>
        //<returns>Say Hello!</returns>
        internal static void GetWelcome()
        {
            Console.WriteLine(Notifications.EnterName);

            string name = String.Empty;
            bool findNumber = true;

            // If find numbers in the name then repeat the action.
            while (findNumber)
            {
                name = Console.ReadLine();

                foreach (char sumbol in name)
                {
                    if (char.IsNumber(sumbol))
                    {
                        findNumber = true;
                        break;
                    }
                    else
                    {
                        findNumber = false;
                    }
                }

                if (findNumber)
                {
                    Console.WriteLine(Notifications.TryAgain);
                    Console.WriteLine();
                    Console.WriteLine(Notifications.EnterName);
                }
            }

            Console.WriteLine();
            Console.WriteLine(Notifications.JustHello + name + "!");
            Console.ReadKey();
        }
    }
}
