﻿using System;
using Gtk;

public partial class MainWindow : Gtk.Window
{
    public MainWindow() : base(Gtk.WindowType.Toplevel)
    {
        Build();
        label2.Visible = false;
    }

    protected void OnDeleteEvent(object sender, DeleteEventArgs a)
    {
        Application.Quit();
        a.RetVal = true;
    }

    protected void OnButton2Clicked(object sender, EventArgs e)
    {
        string userInput = entry1.Text;
        bool findNumber = true;

        if (userInput != null || findNumber)
        {
            foreach (char sumbol in userInput)
            {
                if (char.IsNumber(sumbol))
                {
                    findNumber = true;
                    break;
                }
                else
                {
                    findNumber = false;
                }
            }

            if (userInput != null && !findNumber)
            {
                label2.Text = "Hello, " + userInput + "!";
            }
            else
            {
                label2.Text = "Incorrect name, Try again!";
            }
        }

        label2.Show();
    }
}
