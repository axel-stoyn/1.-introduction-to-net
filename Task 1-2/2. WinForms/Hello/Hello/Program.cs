﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using Hello.Notifications;

namespace Hello
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            //Application.SetCompatibleTextRenderingDefault(false);
            Application.Run(new Form1());
        }

        //public void Welcome()
        //{
        //    Console.WriteLine(ErrorOrIntroduceNotifications.EnterName);

        //    string name = String.Empty;
        //    bool findNumber = true;

        //    // If find numbers in the name then repeat the action.
        //    while (findNumber)
        //    {
        //        name = Console.ReadLine();

        //        foreach (char sumbol in name)
        //        {
        //            if (char.IsNumber(sumbol))
        //            {
        //                findNumber = true;
        //                break;
        //            }
        //            else
        //            {
        //                findNumber = false;
        //            }
        //        }

        //        if (findNumber)
        //        {
        //            Console.WriteLine(ErrorOrIntroduceNotifications.TryAgain);
        //            Console.WriteLine();
        //            Console.WriteLine(ErrorOrIntroduceNotifications.EnterName);
        //        }
        //    }

        //    Console.WriteLine();
        //    Console.WriteLine(ErrorOrIntroduceNotifications.JustHello + name + "!");
        //    Console.ReadKey();

        //}
    }
}
