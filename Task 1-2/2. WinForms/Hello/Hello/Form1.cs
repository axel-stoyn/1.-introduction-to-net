﻿using Hello.Notifications;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Hello
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public new string Name = String.Empty;

        private void button1_Click(object sender, EventArgs e)
        {
            bool findNumber = true;
            foreach (char sumbol in Name)
            {
                if (char.IsNumber(sumbol))
                {
                    findNumber = true;
                    break;
                }
                else
                {
                    findNumber = false;
                }
            }
            if (Name != null && !findNumber)
            {
                MessageBox.Show(ErrorOrIntroduceNotifications.JustHello + Name + "!");
            }
            else
            {
                MessageBox.Show(ErrorOrIntroduceNotifications.TryAgain);
            }
        }

        public void textBox1_TextChanged(object sender, EventArgs e)
        {
            Name = textBox1.Text;
        }
    }
}
