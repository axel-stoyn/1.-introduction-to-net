﻿using Android.App;
using Android.Widget;
using Android.OS;
using System;

namespace Hello
{
    [Activity(Label = "Hello", MainLauncher = true, Icon = "@mipmap/icon")]
    public class MainActivity : Activity
    {
        Button btn1 = null;
        EditText ed1 = null;
        TextView tw1 = null;

        //int count = 1;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);

            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.Main);

            btn1 = FindViewById<Button>(Resource.Id.button1);
            ed1 = FindViewById<EditText>(Resource.Id.editText1);
            tw1 = FindViewById<TextView>(Resource.Id.textView1);
            // Get our button from the layout resource,
            // and attach an event to it
            //Button button = FindViewById<Button>(Resource.Id.myButton);

            btn1.Click += Btn1_Click;
        }

        private void Btn1_Click(object sender, EventArgs e)
        {
            bool findNumber = true;
            foreach (char sumbol in ed1.Text)
            {
                if (char.IsNumber(sumbol))
                {
                    findNumber = true;
                    break;
                }
                else
                {
                    findNumber = false;
                }
            }

            if (ed1.Text != null && !findNumber)
            {
                tw1.Text = "Hello " + ed1.Text + " !";
            }
            else
            {
                tw1.Text = "Incorrect name! Try Again!";
            }
        }
    }
}

