﻿using System;

namespace Hello
{
    class Program
    {
        static void Main(string[] args)
        {
            string name = String.Empty;

            bool findNumber = true;

            while (name == "" || findNumber)
            {
                Console.WriteLine(Notifications.EnterName);
                name = Console.ReadLine();

                // Check name
                findNumber = Welcome.GetWelcome(name);

                if (findNumber)
                {
                    Console.WriteLine(Notifications.TryAgain);
                    Console.WriteLine();
                }
            }

            Console.WriteLine();
            Console.WriteLine(Notifications.JustHello + name + "!");
            Console.ReadKey();
        }
    }
}
