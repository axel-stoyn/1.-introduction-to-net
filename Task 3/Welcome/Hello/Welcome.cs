﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Hello
{
    ///<summary>
    ///Introduce person.
    ///</summary>
    public static class Welcome
    {
        ///<summary>
        ///Get Welcom message.
        ///</summary>
        ///<returns>Check name</returns>
        public static bool GetWelcome(string name)
        {
            bool findNumber = true;

                foreach (char sumbol in name)
                {
                    if (char.IsNumber(sumbol))
                    {
                        findNumber = true;
                        break;
                    }
                    else
                    {
                        findNumber = false;
                    }
                }

            return findNumber;
        }
    }
}
