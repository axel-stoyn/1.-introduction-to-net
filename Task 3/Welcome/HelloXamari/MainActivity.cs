﻿using Android.App;
using Android.OS;
using Android.Support.V7.App;
using Android.Widget;
using System;

namespace HelloXamari
{
    [Activity(Label = "@string/app_name", Theme = "@style/AppTheme", MainLauncher = true)]
    public class MainActivity : AppCompatActivity
    {
        Button btn1 = null;
        EditText ed1 = null;
        TextView tw1 = null;

        protected override void OnCreate(Bundle savedInstanceState)
        {
            base.OnCreate(savedInstanceState);
            // Set our view from the "main" layout resource
            SetContentView(Resource.Layout.activity_main);

            btn1 = FindViewById<Button>(Resource.Id.button1);
            ed1 = FindViewById<EditText>(Resource.Id.editText1);
            tw1 = FindViewById<TextView>(Resource.Id.textView1);

            btn1.Click += Btn1_Click;
        }

        ///<summary>
        ///Get Welcom message.
        ///</summary>
        ///<returns>Get welcome message when click.</returns>
        private void Btn1_Click(object sender, EventArgs e)
        {
            // Check name.
            bool findNumber = true;
            foreach (char sumbol in ed1.Text)
            {
                if (char.IsNumber(sumbol))
                {
                    findNumber = true;
                    break;
                }
                else
                {
                    findNumber = false;
                }
            }

            if (ed1.Text != null && !findNumber)
            {
                tw1.Text = "Hello " + ed1.Text + " !";
            }
            else
            {
                tw1.Text = "Incorrect name! Try Again!";
            }
        }
    }
}