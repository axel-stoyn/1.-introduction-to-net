﻿using System;
using System.Windows.Forms;

namespace HelloWinForms
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public new string Name = String.Empty;

        ///<summary>
        ///Get Welcom message.
        ///</summary>
        ///<returns>Check name</returns>
        private void button1_Click(object sender, EventArgs e)
        {
            // Check name
            bool findNumber = true;
            foreach (char sumbol in Name)
            {
                if (char.IsNumber(sumbol))
                {
                    findNumber = true;
                    break;
                }
                else
                {
                    findNumber = false;
                }
            }

            if (Name != "" && !findNumber)
            {
                MessageBox.Show(Notifications.JustHello + Name + "!");
            }
            else
            {
                MessageBox.Show(Notifications.TryAgain);
            }
        }

        public void textBox1_TextChanged(object sender, EventArgs e)
        {
            Name = textBox1.Text;
        }
    }
}
